﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using File = TagLib.File;

namespace Library
{
	[Serializable()]
	public class M_music : Imedia
	{
		public String path;
		public String Artist;
		public String Album;
		public String Title;
		public String Duration;
		public String Year;
		public String Extensions;
		public String Size;

		public M_music(String PathFile)
		{
			path = PathFile;
			if (path != null)
			{
				var music = File.Create(PathFile);
				Album = music.Tag.Album;
				Title = music.Tag.Title;
				Year = music.Tag.Year.ToString();
				Artist = music.Tag.FirstAlbumArtist;
				Duration = music.Properties.Duration.ToString();
//				Size = music.Length.ToString();
				FileInfo f = new FileInfo(path);
				Size = f.Length.ToString();
			}
		}

		public M_music()
		{
			Artist = "unknown";
			Album = "unknown";
			Title = "unknown";
			Year = "unknown";
		}

		static public M_music M_music_cpy(Imedia cpy)
		{
			return (new M_music(cpy.getPath()));
		}


		public void show()
		{
			Console.Write("Album = " + Album + "\n");
			Console.Write("Titre = " + Title + "\n");
			Console.Write("Année = " + Year + "\n");
			Console.Write("Artiste = " + Artist + "\n\n");
		}

		public string Wh_media()
		{
			return ("Music");
		}

		public String getName()
		{
			return (Path.GetFileName(path));
		}

		public String getPath()
		{
			return (path);
		}

		public String getExt()
		{
			return (Path.GetExtension(path));
		}
	}
}
