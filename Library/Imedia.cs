﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Library
{
	public interface Imedia
	{		
		String getName();
		String getPath();
		String getExt();
		String Wh_media();
	}
}
