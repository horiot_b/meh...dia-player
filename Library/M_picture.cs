﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using File = TagLib.File;

namespace Library
{
	[Serializable()]
	public class M_picture : Imedia
	{
		public String Name;
		public String Resolution;
		public String path;
		public String Size;

		public M_picture(String PathFile)
		{
			path = PathFile;
			if (path != null)
			{
				Image img = Image.FromFile(path);
				Resolution = img.Width + "x" + img.Height;
				Name = Path.GetFileName(path);
				FileInfo f = new FileInfo(path);
				Size = f.Length.ToString();
			}
		}

		public M_picture()
		{
			Name = "unknown";
			Resolution = "0x0";
		}

		static public M_picture M_picture_cpy(Imedia cpy)
		{
			return (new M_picture(cpy.getPath()));
		}

		public string Wh_media()
		{
			return ("Picture");
		}

		public String getPath()
		{
			return (path);
		}

		public string getExt()
		{
			return (Path.GetExtension(path));
		}

		public string getName()
		{
			return (Name);
		}

		string Imedia.getPath()
		{
			return (path);
		}
	}
}
