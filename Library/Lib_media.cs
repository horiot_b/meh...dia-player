﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
	public class Lib_media
	{
		public  List<M_music> lib_music;
		public  List<M_video> lib_video;
		public  List<M_picture> lib_picture;

		public Lib_media()
		{
			lib_music = new List<M_music>();
			lib_video = new List<M_video>();
			lib_picture = new List<M_picture>();
		}

		public void Add(Imedia elem)
		{
			String type = elem.Wh_media();
			switch (type)
			{
				case "Picture":
					lib_picture.Add(M_picture.M_picture_cpy(elem));
					break;
				case "Video":
					lib_video.Add(M_video.M_videos_cpy(elem));
					break;
				case "Music":
					lib_music.Add(M_music.M_music_cpy(elem));
					break;
			}
		}

		public void show()
		{
			foreach (M_music elem in lib_music)
			{
				Console.WriteLine(elem.Wh_media() + " : " + elem.getPath());
			}

			foreach (M_picture elem in lib_picture)
			{
				Console.WriteLine(elem.Wh_media() + " : " + elem.getPath());
			}

			foreach (M_video elem in lib_video)
			{
				Console.WriteLine(elem.Wh_media() + " : " + elem.getPath());
			}
		}
	}
}
