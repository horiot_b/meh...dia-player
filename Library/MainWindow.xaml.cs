﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PlayList;

namespace Library
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
		Lib lib;
    DataTable table;
    DataTable table2;
    DataTable table3;

    public MainWindow()
    {
      table = Init_Table();
      table2 = Init_Table();
      table3 = Init_Table();
      table3.Columns.Remove("Duration");

      InitializeComponent();
			lib = new Lib();
			Update_lib();
    }

		private DataTable Init_Table()
		{
			DataTable table = new DataTable();

			table.Columns.Add("Name", typeof(string));
			table.Columns.Add("Duration", typeof(string));
			table.Columns.Add("Format", typeof(string));
			table.Columns.Add("Path", typeof(string));
			table.Columns.Add("Size (in bytes)", typeof(long));
			return table;
		}
		public void Update_lib()
		{
      while (table.Rows.Count > 0)
      {
        table.Rows.RemoveAt(0);
      }
      while (table2.Rows.Count > 0)
      {
        table2.Rows.RemoveAt(0);
      }
      while (table3.Rows.Count > 0)
      {
        table3.Rows.RemoveAt(0);
      }
			Lib_media main = lib.get_mainlib();

			foreach (M_music elem in main.lib_music)
			{
				table.Rows.Add(elem.getName(), elem.Duration, elem.getExt(), elem.getPath(), elem.Size);
			}
			MusicDataGrid.DataContext = table.DefaultView;

			foreach (M_video elem in main.lib_video)
			{
				table2.Rows.Add(elem.getName(), elem.Duration, elem.getExt(), elem.getPath(), elem.Size);
			}
			VideoDataGrid.DataContext = table2.DefaultView;

			foreach (M_picture elem in main.lib_picture)
			{
				table3.Rows.Add(elem.getName(), elem.getExt(), elem.getPath(), elem.Size);
			}
			ImageDataGrid.DataContext = table3.DefaultView;
		}

    private void MusicDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      foreach (DataRowView datarow in this.MusicDataGrid.SelectedItems)
      {
        this.playlist.Add(new Uri(datarow["Path"].ToString()), datarow["Name"].ToString());
      }
    }

    private void VideoDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      foreach (DataRowView datarow in this.VideoDataGrid.SelectedItems)
      {
        this.playlist.Add(new Uri(datarow["Path"].ToString()), datarow["Name"].ToString());
      }
    }

    private void ImageDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      foreach (DataRowView datarow in this.ImageDataGrid.SelectedItems)
      {
        this.playlist.Add(new Uri(datarow["Path"].ToString()), datarow["Name"].ToString());
      }
    }

		private void MusicDataGrid_MouseRightClick(object sender, MouseButtonEventArgs e)
		{
			foreach (DataRowView datarow in this.MusicDataGrid.SelectedItems)
			{
				lib.Delete_file(datarow["Path"].ToString());
			}
			Update_lib();
		}

		private void VideoDataGrid_MouseRightClick(object sender, MouseButtonEventArgs e)
		{
			foreach (DataRowView datarow in this.VideoDataGrid.SelectedItems)
			{
				lib.Delete_file(datarow["Path"].ToString());
			}
			Update_lib();
		}

		private void ImageDataGrid_MouseRightClick(object sender, MouseButtonEventArgs e)
		{
			foreach (DataRowView datarow in this.ImageDataGrid.SelectedItems)
			{
				lib.Delete_file(datarow["Path"].ToString());
			}
			Update_lib();
		}

    private void BackToPlayer(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

		private void OnOpenFolder(Object s, RoutedEventArgs e)
		{
			/*FolderBrowserDialog file = new FolderBrowserDialog();
			file.ShowDialog();*/
		}

    private void OnOpen(object s, RoutedEventArgs e)
    {
      OpenFileDialog file = new OpenFileDialog();
      file.Multiselect = true;
      file.AddExtension = true;
      file.DefaultExt = "*.*";
      file.Filter = "Media(*.*)|*.mp3;*.mp4;*.wmv;*.wma;*.png;*.jpg;*.jpeg;*.wav;*.avi;";
      file.ShowDialog();


      try
      {
        foreach (string filename in file.FileNames)
        {
          lib.add_one_file(filename);
          this.playlist.Add(new Uri(filename), filename);
        }
        Update_lib();
      }
      catch (Exception ex)
      {
        Trace.WriteLine("Failed " + ex.ToString());
      };
    }

    private void Load_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog file = new OpenFileDialog();
      file.Filter = "Playlist(*.xml*)|*.xml*";
      file.ShowDialog();
      if (file.FileName != null)
        playlist.Deserializer(file.FileName);
    }

    private void Save_Click(object sender, RoutedEventArgs e)
    {
      SaveFileDialog file = new SaveFileDialog();
      file.Filter = "Playlist(*.xml*)|*.xml*";
      file.AddExtension = true;
      file.DefaultExt = ".xml";
      file.ShowDialog();
      if (file.FileName != null)
        playlist.Serializer(file.FileName);
    }

    private void Media_Drop(object s, DragEventArgs e)
    {
			Console.WriteLine("WTF !!!!!!");
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
      {
				Console.WriteLine("WTF 2!!!!!!");
				string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
        for (int i = 0; i < files.Length; i++)
        {
					Console.WriteLine("WTF 3!!!!!!");
					Console.WriteLine("My files = " + files[i].ToString());
          this.playlist.Add(new Uri(files[i]), files[i]);
        }
      }
    }

		~MainWindow()
		{
			lib.Serializer();
		}
  }
}
