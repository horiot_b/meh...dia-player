﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Diagnostics;
using TagLib;


/*
 * allumage
 * deserialize
 * choisir si checker lib.
 * 
 */
namespace Library
{
	public class Lib
	{
		String dos = @".\Dossiers_surveillés.txt";
		String[] list_audio = { ".mp3", ".aac", ".ogg", ".flac", ".wma", ".wav" };
		String[] list_vid = { ".avi", ".mpeg", ".mp4", ".wmv"};
		String[] list_pic = { ".png", ".gif", ".jpeg", ".jpg"};
		String serialized_file = @".\saved_lib.xml";
		List<String> list_w = new List<string>();
		Lib_media main_lib = new Lib_media();

    public Lib()
    {
      try
      {
        if (System.IO.File.Exists(dos) == false)
          System.IO.File.Create(dos);
        list_w = System.IO.File.ReadAllLines(dos).ToList<String>();

        if (System.IO.File.Exists(serialized_file) == false)
          System.IO.File.Create(serialized_file);
        if (new FileInfo(serialized_file).Length == 0)
        {
          init_db();
          Serializer();
        }
        else
          Deserializer();
      }
      catch (Exception ex)
      {
        Trace.WriteLine("Failed " + ex.ToString());
      }
    }

		public Lib_media get_mainlib()
		{ return main_lib; }

		public void Add_file(String path)
		{
				using (StreamWriter rd = new StreamWriter(dos))
				{
					rd.WriteLine(path);
					rd.Close();
				}
				list_w.Add(path);			
		}

		public void Delete_file(String path)
		{
			int ret = is_media(Path.GetExtension(path));
			switch (ret)
			{
				case 1:
					foreach (M_music elem in main_lib.lib_music)
					{
						if (elem != null && elem.getPath() == path)
						{
							main_lib.lib_music.Remove(elem);
							break;
						}
					}
					break;
				case 2:
					foreach (M_picture elem in main_lib.lib_picture)
					{
						if (elem != null && elem.getPath() == path)
						{
							main_lib.lib_picture.Remove(elem);
							break;
						}
					}

					break;
				case 3:
					foreach (M_video elem in main_lib.lib_video)
					{
						if (elem != null && elem.getPath() == path)
						{
							main_lib.lib_video.Remove(elem);
							break;
						}
					}
					break;
			}
			Serializer();
		}

		public void show_lib()
		{
			main_lib.show();
		
		}

		private int is_media(String ext)
		{
			if (list_audio.Contains(ext) == true)
				return 1;
			else if (list_pic.Contains(ext) == true)
				return 2;
			else if (list_vid.Contains(ext) == true)
				return 3;
			return -1;
		}

		public void add_one_file(String f)
		{
			String ext;			
			ext = Path.GetExtension(f);
			int ret = is_media(ext);
			Imedia tmp;
			switch (ret)
			{
				case 1:
					tmp = new M_music(@f);
					break;
				case 2:
					tmp = new M_picture(@f);
					break;
				case 3:
					tmp = new M_video(@f);
					break;
				default:
					tmp = null;
					break;
			}
			if (tmp != null)
				main_lib.Add(tmp);
			Serializer();
		}

		public void fullfill_the_db(String Name)
		{			
			try
			{
				foreach (string d in Directory.GetDirectories(Name))
				{
					foreach (string f in Directory.GetFiles(d))
					{
						add_one_file(f);
					}
					fullfill_the_db(d);
				}
			}
			catch (Exception e)
			{
				Trace.WriteLine("Fail: " + e.ToString());
			}
		}		

		public void init_db()
		{
			foreach (String file_lib in list_w)
			{
				Console.Write(file_lib + "\n");
				fullfill_the_db(file_lib);				
			}
		}

		public void Serializer()
		{
			if (System.IO.File.Exists(serialized_file) == false)
				System.IO.File.Create(serialized_file);

			try
			{
				XmlSerializer sx = new XmlSerializer(typeof(Lib_media));
				using (StreamWriter wr = new StreamWriter(serialized_file))
				{
				//	wr.Flush();
					sx.Serialize(wr,main_lib);
					wr.Close();
				}
			}
			catch (Exception e)
			{
				Trace.WriteLine("Fail: " + e.ToString());
			}
		}

		public void Deserializer()
		{
			if (System.IO.File.Exists(serialized_file) == false)
				System.IO.File.Create(serialized_file);
			XmlSerializer xs;
			try
			{
				xs = new XmlSerializer(typeof(Lib_media));
				using (StreamReader rd = new StreamReader(serialized_file))
				{
					try
					{
						main_lib = xs.Deserialize(rd) as Lib_media;
					}
					catch (Exception e)
					{
						Trace.WriteLine("Fail: " + e.ToString());
					}
					rd.Close();
				}
			}
			catch (Exception e)
			{
				  if (e.InnerException != null)
            Console.WriteLine("Inner exception: {0}", e.InnerException);      
			}			
		}

		~Lib()
		{
			Serializer();
		}

	}
}
