﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using File = TagLib.File;

namespace Library
{
	[Serializable()]
	public class M_video : Imedia
	{
		public String Duration;
		public String Title;
		public String Resolution;
		public String path;
		public String Size;

		public M_video(String PathFile)
		{
			path = PathFile;
			if (path != null)
			{
				var video = TagLib.File.Create(path);
				Title = Path.GetFileName(path);
				Resolution = video.Properties.VideoWidth.ToString() + "x" + video.Properties.VideoHeight.ToString();
				Duration = video.Properties.Duration.ToString();
				FileInfo f = new FileInfo(path);
				Size = f.Length.ToString();
			}
		}

		public M_video()
		{
			Duration = "unknown";
			Title = "unknown";
			Resolution = "0x0";
		}

		static public M_video  M_videos_cpy(Imedia cpy)
		{
			return (new M_video(cpy.getPath()));
		}

		public string Wh_media()
		{
			return ("Video");
		}

		public string getName()
		{
			return (Title);
		}

		public string getPath()
		{
			return (path);
		}

		public string getExt()
		{
			return (Path.GetExtension(path));
		}
	}
}
