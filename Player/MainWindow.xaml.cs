﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private enum States
        {
            None,
            Playing,
            Stoped,
            Paused
        }

        private double WSize;
        private double HSize;
        private States State = States.None;

        #region Window
        public MainWindow()
        {
            InitializeComponent();
            WSize = Width;
            HSize = Height;

            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
            this.CurrentTime.Text = "0";
            this.TotalTime.Text = "0";
        }

        private void MainWindow_Loaded(object s, RoutedEventArgs e)
        {
            // init library
            WindowState = System.Windows.WindowState.Normal;
            UpdateLayout();
            System.Windows.Threading.DispatcherTimer dispTimer = new System.Windows.Threading.DispatcherTimer();
            dispTimer.Tick += new EventHandler(timer_Tick);
            dispTimer.Interval = new TimeSpan(0, 0, 1);
            dispTimer.Start();
        }

        #endregion

        #region Controls
        private void OnOpen(object s, RoutedEventArgs e)
        {
            State = States.Stoped;
            OpenFileDialog file = new OpenFileDialog();
            file.Multiselect = true;
            file.AddExtension = true;
            file.DefaultExt = "*.*";
            file.Filter = "Media(*.*)|*.mp3;*.mp4;*.wmv;*.wma;*.png;*.jpg;*.jpeg;*.wav;*.avi;";
            file.ShowDialog();


            try
            {
                if (playlist.Empty())
                    Media.Source = new Uri(file.FileNames[0]);
                foreach (string filename in file.FileNames)
                {
                    this.playlist.Add(new Uri(filename), filename);
                }

            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed " + ex.ToString());
            };
            if (State != States.Playing)
            {
                Media.Play();
                this.State = States.Playing;
            }
        }

        void timer_Tick(object s, EventArgs e)
        {
            Time.Value = Media.Position.TotalSeconds;
        }

        private void Time_ValueChanged(object sr, RoutedPropertyChangedEventArgs<double> e)
        {
            TimeSpan s = TimeSpan.FromSeconds(e.NewValue);
            Media.Position = s;
            CurrentTime.Text = s.ToString(@"hh\:mm\:ss");
        }

        private void Sound_ValueChanged(object s, RoutedPropertyChangedEventArgs<double> e)
        {
            Media.Volume = e.NewValue / 10;
        }

        private void Media_MediaOpened(object s, RoutedEventArgs e)
        {
            if (Media.NaturalDuration.HasTimeSpan)
            {
                TotalTime.Text = Media.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss");
                TimeSpan ts = TimeSpan.FromMilliseconds(Media.NaturalDuration.TimeSpan.TotalMilliseconds);
                Time.Maximum = ts.TotalSeconds;
            }
            else
                Time.Maximum = 0;
            Sound.Maximum = 10;
            Sound.Value = Media.Volume * 10;
        }

        private void OnPause(object s, RoutedEventArgs e)
        {
            Media.Pause();
            State = States.Paused;
        }

        private void OnStop(object s, RoutedEventArgs e)
        {
            Media.Stop();
            State = States.Stoped;
        }

        private void OnPlay(object s, RoutedEventArgs e)
        {
            Media.Play();
            State = States.Playing;
        }

        private void OnResize(object s, RoutedEventArgs e)
        {
            Media.Height = Media.Height * MainWindowE.Height / HSize;
            Media.Width = Media.Width * MainWindowE.Width / WSize;
            HSize = MainWindowE.Height;
            WSize = MainWindowE.Width;
        }

        private void OnQuit(object s, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MainWindow_KeyDown(object s, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftCtrl:
                    if (HidePanel.Opacity == 0)
                        HidePanel.Opacity = 100;
                    else if (HidePanel.Opacity != 0)
                        HidePanel.Opacity = 0;
                    break;
                case Key.F11:
                    if (WindowState != System.Windows.WindowState.Maximized || WindowStyle != System.Windows.WindowStyle.None)
                    {
                        WindowStyle = System.Windows.WindowStyle.None;
                        ResizeMode = ResizeMode.NoResize;
                        WindowState = WindowState.Maximized;
                    }
                    else
                    {
                        WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
                        WindowState = WindowState.Normal;
                        ResizeMode = ResizeMode.CanResize;
                    }
                    break;
                case Key.Space:
                    if (State == States.Playing)
                    {
                        State = States.Paused;
                        Media.Pause();
                    }
                    else if (State != States.None)
                    {
                        Media.Play();
                        State = States.Playing;
                    }   
                    break;
            }
        }

        private void Media_Drop(object s, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                try
                {
                    Media.Source = new Uri(files[0]);
                }
                catch (UriFormatException ex)
                {
                  Trace.WriteLine("Failed " + ex.ToString());
                };
                for (int i = 0; i < files.Length; i++)
                {
                    this.playlist.Add(new Uri(files[i]), files[i]);
                }
            }
        }

        private void HidePanel_MouseEnter(object s, MouseEventArgs e)
        {
            HidePanel.Opacity = 100;
        }

        private void HidePanel_MouseLeave(object s, MouseEventArgs e)
        {
            HidePanel.Opacity = 0;
        }

        private void OnPrevious(object s, RoutedEventArgs e)
        {
            Media.Stop();
            if (this.playlist.SelectedIndex > 0)
                this.playlist.SelectedIndex = this.playlist.SelectedIndex - 1;
            else if (this.playlist.SelectedIndex == 0)
                this.playlist.SelectedIndex = this.playlist.Items.Count - 1;
            Media.Source = this.playlist.getUri(this.playlist.SelectedItem.ToString());
            Media.Play();
            this.State = States.Playing;
        }

        private void OnNext(object s, RoutedEventArgs e)
        {
            Media.Stop();
            if (this.playlist.SelectedIndex < this.playlist.Items.Count - 1)
                this.playlist.SelectedIndex = this.playlist.SelectedIndex + 1;
            else if (this.playlist.SelectedIndex == this.playlist.Items.Count - 1)
                this.playlist.SelectedIndex = 0;
            Media.Source = this.playlist.getUri(this.playlist.SelectedItem.ToString());
            Media.Play();
            this.State = States.Playing;
        }

        private void OnDoubleClickPlaylist(object s, RoutedEventArgs e)
        {
            Media.Source = this.playlist.getUri(this.playlist.SelectedItem.ToString());
            Media.Play();
            this.State = States.Playing;
        }
        #endregion

        #region Interface
        private void playlist_MouseLeave(object sender, MouseEventArgs e)
        {
            playlist.Opacity = 0;
        }

        private void playlist_MouseEnter(object sender, MouseEventArgs e)
        {
            playlist.Opacity = 100;
        }

        private void Menu_MouseLeave(object sender, MouseEventArgs e)
        {
            Menu.Opacity = 0;
        }

        private void Menu_MouseEnter(object sender, MouseEventArgs e)
        {
            Menu.Opacity = 100;
        }

        private void OnLibrary(object sender, RoutedEventArgs e)
        {
          Window main = new Library.MainWindow();
          App.Current.MainWindow = main;
          main.Show();
        }

        private void Mute_Checked(object sender, RoutedEventArgs e)
        {
            Media.IsMuted = true;
        }

        private void Mute_Unchecked(object sender, RoutedEventArgs e)
        {
            Media.IsMuted = false;
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Playlist(*.xml*)|*.xml*";
            file.ShowDialog();
            if (file.FileName != null)
                playlist.Deserializer(file.FileName);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog file = new SaveFileDialog();
            file.Filter = "Playlist(*.xml*)|*.xml*";
            file.AddExtension = true;
            file.DefaultExt = ".xml";
            file.ShowDialog();
            if (file.FileName != null)
                playlist.Serializer(file.FileName);
        }
        #endregion
    }
}
