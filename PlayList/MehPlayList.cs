﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows;
using ErrorWindow;

namespace PlayList
{
  public class MehPlayList : System.Windows.Controls.ListBox
  {
    private Dictionary<String, Uri> Files;
    private Dictionary<String, String> FilePaths;

		public List<string> RealPaths;
		public List<string> Paths;
    public void Add(Uri path, string Name)
    {
      if (this.Files == null)
      {
        this.Files = new Dictionary<string, Uri>();
        this.FilePaths = new Dictionary<string, string>();
        this.Paths = new List<string>();
				this.RealPaths = new List<string>();
      }
      this.Files[Name] = path;
			this.RealPaths.Add(path.AbsolutePath);
			this.Paths.Add(Name);
      this.FilePaths[Path.GetFileNameWithoutExtension(Name)] = Name;
      this.Items.Add(Path.GetFileNameWithoutExtension(Name));
    }

    public Uri getUri(string name)
    {
      try
      {
        return this.Files[FilePaths[name]];
      }
      catch (Exception ex)
      {
        Trace.Write("Failed : " + ex);
        return null;
      }
    }

    public Boolean Empty()
    {
      return (this.Files == null ? true : false);
    }

    public void Serializer(String FileName)
    {
      try
      {
        XmlSerializer xs = new XmlSerializer(typeof(List<string>));
        using (StreamWriter wr = new StreamWriter(FileName))
        {
          xs.Serialize(wr, Paths);
        }
      }
      catch (Exception e)
      {
        Trace.WriteLine("Fail: " + e.ToString());
      }
    }

    public void Deserializer(String FileName)
    {
      if (FileName != null && FileName != "")
      {
        Paths = new List<String>();
        Files = new Dictionary<string, Uri>();
        FilePaths = new Dictionary<string, string>();
        XmlSerializer xs = new XmlSerializer(typeof(List<string>));
        using (StreamReader rd = new StreamReader(FileName))
        {
          try
          {
            Paths = xs.Deserialize(rd) as List<String>;
          }
          catch (Exception e)
          {
            Window Error = new ErrorWindow.Error(e.GetType().ToString());
            Error.Show();
          }
        }
        while (!Items.IsEmpty)
        {
          Items.RemoveAt(0);
        }
        foreach (string s in Paths)
        {
          Files[s] = new Uri(s);
          this.FilePaths[Path.GetFileNameWithoutExtension(s)] = s;
          Items.Add(Path.GetFileNameWithoutExtension(s));
        }
      }
    }
  }
}
